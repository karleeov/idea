const path = require('path');

const express = require('express');
const cros = require('cors');
require('dotenv').config();
const router = express.Router();

const port = process.env.PORT || 5000;
const connectDB = require('./config/db');

connectDB();

const app = express();

app.use(express.static(path.join(__dirname, 'public')));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(
  cros({
    origin: ['http://localhost:8000', 'http://localhost:3000'],
    credentials: true,
  })
);
router.get('/', (req, res) => {
  res.send(`hello ${port}`);
});

const ideaRouter = require('./routes/ideas');

app.use('/api/ideas', ideaRouter);

app.listen(port, () => console.log(`working ${port}`));
