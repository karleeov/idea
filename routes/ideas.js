const express = require('express');
const router = express.Router();

const Idea = require('../models/Idea');

const ideas = [
  {
    id: 1,
    text: 'hello world',
    tag: 'tech',
    username: 'haha',
    date: '2019-01-01',
  },
  {
    id: 2,
    text: 'hesadadsallo world',
    tag: 'smart',
    username: 'haha',
    date: '2019-01-01',
  },
  {
    id: 3,
    text: 'hahahah3  world',
    tag: 'home',
    username: 'haha',
    date: '2019-01-01',
  },
];

router.get('/', async (req, res) => {
  try {
    const ideas = await Idea.find();
    console.log(ideas);
    res.json({ success: true, data: ideas });
  } catch (err) {
    res.status(500).json({ success: false, msg: 'server error' });
  }

  // res.json({ success: true, data: ideas });
});

router.get('/:id', (req, res) => {
  const idea = Idea.find((Idea) => Idea.id === +req.params.id);
  if (!idea) {
    return res
      .status(405)
      .json({ success: false, msg: `no idea with id ${req.params.id}` });
  }

  res.json({ success: true, data: idea });
});

router.put('/:id', async (req, res) => {
  try {
    const updatedidea = await Idea.findByIdAndUpdate(
      req.params.id,
      {
        $set: {
          text: req.body.text,
          tag: req.body.tag,
          username: req.body.username,
        },
      },
      { new: true }
    );
    res.json({ sucess: true, data: updatedidea });
  } catch (err) {
    console.log(error);
    res.status(500).json({ success: false, msg: 'put server error' });
  }
});

router.get('/', (req, res) => {
  res.json({ success: true, data: ideas });
});

router.get('/:id', async (req, res) => {
  try {
    const allidea = await Idea.findById(req.params.id);
    res.json({ success: true, data: allidea });
  } catch (err) {
    res.status(500).json({ success: false, msg: 'server error' });
  }
});

router.post('/', async (req, res) => {
  const idea = new Idea({
    text: req.body.text,
    tag: req.body.tag,
    username: req.body.username,
  });

  try {
    const savedIdea = await idea.save();
    res.send({ sucess: true, data: savedIdea });
  } catch (err) {
    res.status(500).json({ success: false, msg: 'server error' });
  }
});

router.delete('/:id', async (req, res) => {
  try {
    await Idea.findByIdAndDelete(req.params.id);
    res.json({ success: true, msg: 'deleted', data: {} });
  } catch (error) {
    res.status(500).json({ success: false, msg: 'server error' });
  }
});

module.exports = router;
