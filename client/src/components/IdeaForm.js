import IdeasApi from '../services/ideasApi';
import IdeaList from './IdeaList';

class IdeaForm {
  constructor() {
    this._formModel = document.querySelector('#form-modal');
    this._ideaList = new IdeaList();
  }
  addEventListeners() {
    this._form.addEventListener('submit', this.handleSubmit.bind(this));
  }

  async handleSubmit(e) {
    e.preventDefault();

    if (
      !this._form.elements.text.value ||
      !this._form.elements.tag.value ||
      !this._form.elements.username.value
    ) {
      alert('Please fill out all fields');
      return;
    }

    localStorage.setItem('username');

    console.log('SUBMIT');
    const idea = {
      text: this._form.elemenets.text.value,
      tag: this._form.elemenets.tag.value,

      username: this._form.elemenets.username.value,
    };

    const newIdea = await IdeasApi.createIdea(idea);
    this._ideaList.addIdeaList(newIdea.data.data);
    console.log(newIdea);
    this.render();
    document.dispatchEvent(new Event('closemodal'));
  }

  render() {
    this._formModel.innerHTML = `
    <form id="idea-form">
    <div class="form-control">
      <label for="idea-text">Enter a Username</label>
      <input type="text" name="username" id="username" value="${
        localStorage.getItem('username') ? localStorage.getItem('username') : ''
      }"/>
    </div>
    <div class="form-control">
      <label for="idea-text">What's Your Idea?</label>
      <textarea name="text" id="idea-text"></textarea>
    </div>
    <div class="form-control">
      <label for="tag">Tag</label>
      <input type="text" name="tag" id="tag" />
    </div>
    <button class="btn" type="submit" id="submit">Submit</button>
  </form>`;

    this.formModel = document.querySelector('#idea-form');
    this.addEventListeners();
  }
}

export default IdeaForm;
