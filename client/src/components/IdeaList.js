import IdeasApi from '../services/ideasApi';

class IdeaList {
  constructor() {
    this.ideaList = document.querySelector('#idea-list');
    this.ideas = [
      // {
      //   id: 1,
      //   username: 'johndoe',
      //   text: 'This is a test idea',
      //   tag: 'health',
      //   date: '2020-01-01',
      // },
      // {
      //   id: 1,
      //   username: 'johndoe',
      //   text: 'This is a test idea',
      //   tag: 'technology',
      //   date: '2020-01-01',
      // },
    ];
    this.getIdeas();
    this._validTags = new Set();
    this._validTags.add('technology');
    this._validTags.add('health');
  }

  async getIdeas() {
    try {
      console.log('try to get');
      const res = await IdeasApi.getIdeas();

      this._ideas = res.data.data;
      console.log(this._ideas, 'working ideas');
      this.render();
    } catch (error) {
      console.log(error, 'cannot get data api');
    }
  }

  addIdeaList(idea) {
    this._ideas.push(idea);
    this.render();
  }

  getTagClass(tag) {
    tag = tag.toLowerCase();
    let tagClass = '';
    if (this._validTags.has(tag)) {
      tagClass = `tag-${tag}`;
    } else {
      tagClass = '';
    }
    return tagClass;
  }

  render() {
    this.ideaList.innerHTML = `
      ${this._ideas
        .map((idea) => {
          const tagClass = this.getTagClass(idea.tag);
          return `
        <div class="card">
        <button class="delete"><i class="fas fa-times"></i></button>
        <h3>
        ${idea.text}
        </h3>
        <p class="tag ${tagClass}">${idea.tag.toUpperCase()}</p>
        <p>
          Posted on <span class="date">${idea.date}</span> by
          <span class="author">${idea.username}</span>
        </p>
      </div>
        `;
        })
        .join('')}
    `;
  }
}

export default IdeaList;
