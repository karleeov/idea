import axios from 'axios';

class IdeasApi {
  constructor() {
    this._apiUrl = '/api/ideas';
  }

  getIdeas() {
    return axios.get(this._apiUrl);
  }
  createIdeas(data) {
    return axios.post(this._apiUrl, data);
  }
}

export default new IdeasApi();
